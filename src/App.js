import React, {useState} from 'react';
import './App.css';
import '../node_modules/bulma/css/bulma.min.css';
import Header from './Components/Header/Header';
import Card from './Components/Card/Card';


function App() {
  
  const [cards, setCards] = useState([
    
  ])

  const [tache, setTache] = useState()
  const [txt, setTxt] = useState()

  function createCard(e){
    e.preventDefault()
    setCards([...cards, {tache: tache, txt: txt}])
    setTache('')
    setTxt('')
  }

  function removeCard(index){
    const clearTab = [...cards];
    setCards(clearTab.filter(item => clearTab.indexOf(item) !== clearTab.indexOf(clearTab[index])))
  }


  return (
    <div>
      <Header />
      <div className="container px-3">
        <h2 className="title mt-5">
          Rentrez vos tache à faire
        </h2>
        <form onSubmit={createCard}>
          <div className="field">
            <div className="control">
              <label htmlFor="tache" className="label">
                Tache
              </label>
              <input
               value={tache}
               type="text"
               className="input"
               id="tache"
               placeholder='une tache à faire..' 
               onChange={(e) => setTache(e.target.value)}
               />
            </div>
          </div>

          <div className="field">
            <div className="control">
              <label htmlFor="txt" className="label">
                Déscription de la tache
              </label>
              <textarea
               value={txt}
               type="text"
               className="textarea"
               id="txt"
               placeholder='Déscription de la tache'
               onChange={(e) => setTxt(e.target.value)} 
               >
              </textarea>
            </div>
          </div>
            <div className="control">
              <button type="submit" className='button is-link'>Créer une tache</button>
            </div>
        </form>
          {
            cards.map((card, index) => {
              return <Card key={index} index={index} tache={card.tache} txt={card.txt} removeFunc = {removeCard} />
            })
          } 
      </div>
    </div>
  );
}

export default App;
